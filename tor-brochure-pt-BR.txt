A Missão do Tor

O Projeto Tor desenvolve e distribui ferramentes livres e abertas que permitem que jornalistas, ativistas dos direitos humanos, diplomatas, empresários(as) e qualquer outra pessoa usem a Internet sem serem vigiados por governos ou empresas.

O Projeto Tor é ainda um recurso mundial para a tecnologia, defesa, investigação e educação na busca por liberdade de expressão, direito à privacidade online e luta contra a censura. O Projeto Tor tem orgulho em ser uma exemplar fundação sem fins lucrativos.

Saiba mais em:
https://www.torproject.org/

Junte-se a nós

Precisamos da sua ajuda para continuar este trabalho mundial na busca da liberdade de expressão, direito à privacidade online e luta contra a censura. Junte-se a nós como um(a) patrocinador(a), voluntário(a), financiador(a), ou em futuros eventos organizados pela equipe do Tor.

O que o Tor Faz de Melhor

Provê privacidade online

Derrota a censura

Protege jornalistas

Protege quem defende os direitos humanos

Protege vítimas de violência doméstica

Mantém os canais de informação online abertos a todos

Trabalha com legisladores(as)

Entra em parceria com instituições acadêmicas e de pesquisa


Agentes da Lei e o Projeto Tor

Quem Usa Tor?

A grande maioria das pessoas que usam o Tor são pessoas comuns que desejam manter o controle sobre sua privacidade online — ou que vivem sob censura e precisam contornar bloqueios na Internet. Criminosos que desejam infringir a lei já têm opções mais eficazes do que o Tor.

Sem Registros, Sem Backdoor

Quem usa o Tor pode confiar na privacidade concedida pela ferramenta. Por definição, o operador de um servidor da rede Tor não consegue expôr o IP de quem a usa. A revisão contínua do código fonte do programa, feita por comunidades acadêmicas e do código aberto, garante que não haja nenhuma "porta dos fundos" no programa Tor.

Denúncias Anônimas

O Tor oferece a forma mais segura para um sistema de denúncias realmente anônimo —  vital para manter canais de comunicação seguros para testemunhas e informantes.

Operações Sob Disfarce

O Tor é usado por agentes da lei e em investigações para monitorizar sites e serviços online suspeitos de forma anônima. Por ocultar a identidade de quem investiga e a sua localização, o Tor é uma ferramente vital para o sucesso das operações sob disfarce.

Como Funciona o Tor

A Alice criptografa o seu pedido online para o Bob três vezes, e envia-o para o primeiro servidor.

O primeiro servidor remove a primeira camada de criptografia, mas não vê consegue saber que o pedido é dirigido ao Bob.

O segundo servidor remove outra camada de criptografia e reencaminha o pedido.

O terceiro servidor remove a última camada de criptografia e entrega o pedido ao Bob, mas não consegue saber que o pedido veio da Alice.

O Bob não sabe que o pedido online foi feito pela Alice, a menos que ela mesma o diga.

Saber Mais

O compromisso do projeto Tor em ensinar inclui agentes da lei e legisladores(as).

A documentação do Tor e os seus canais de suporte são abertos a todas as pessoas.

Aprenda a usar o serviço ExoneraTor para descobrir se um certo IP foi usado por um servidor da rede Tor.

Saiba mais entrando em contato com a equipe de especialistas do Tor.


Os Benefícios do Anonimato Online

A Realidade

Provedores de Serviço de Internet (como a Velox, Virtua, GVT, etc), sites (como Google ou Facebook) e governos usam uma forma comum de vigilância na internet conhecida como rastreamento de endereço IP para monitorizar conversas em redes públicas.

Sites de notícias mostram artigos diferentes baseados na sua localização.

Sites de compras online podem fazer discriminação de preços baseados no seu país de origem.

Uma pessoa comum é vigiada por mais de uma centena de empresas que vendem "perfis" para anunciantes.

A sua atividade em redes sociais pode ser revelada e usada contra você por pessoas mal-intencionadas.

Liberdade

A paisagem da Internet está em constante mudança, e novidades na lei, política e tecnologia ameaçam o seu anonimato como nunca antes, colocando em risco a nossa liberdade de expressão. Países vigiam-se uns aos outros, além dos seus próprios cidadãos, bloqueando sites na internet, inspecionando as ligações, e restringindo o acesso a importantes notícias mundiais.

(Como funciona o Tor: same as above)

#1 em Privacidade Online

O Tor é tecnologia livre, de código aberto, resultado dos mais de 10 anos de pesquisa e desenvolvimento por especialistas em segurança da equipe Tor.

O Tor é uma das mais eficientes tecnologias para proteger a sua privacidade online e garantir que a sua segurança online fique sob seu controle.


Liberdade & Privacidade Online

censura

(Merriam-Webster, 2012)

Ato de mudar ou reprimir discursos ou escritas que sejam considerados subversivos ao bem comum.

Censura Online

A nível global, a censura toma um significado totalmente novo. Restringir o acesso a informação e monitorar o conteúdo de saída é mais comum do que as pessoas pensam. O grupo de investigação de censura da equipe Tor trabalha para construir ferramentas que se mantenham um passo à frente das táticas de censura e providenciam canais abertos de comunicação para todos online. A equipe Tor constrói parcerias para chamar a atenção e educar as pessoas acerca da importância da privacidade online e a liberdade de expressão.

A vigilância na internet é comum e fácil.

Partes da rede podem ser monitorizadas.

A ligação do Bob pode ser observada ou ele pode estar trabalhando para o regime.

A Alice pode ser observada enquanto se liga ao Bob.

(Como funciona o Tor: same as above)

Algumas técnicas para contornar censura usam apenas uma camada para esconder as ligações.

Um proxy pode ser grampeado ou monitorado.

Infelizmente, uma camada (como em um proxy) é fácil de atacar.


