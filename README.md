Tor brochure
============

These brochures are informational pieces to spread the word about Tor to
the general public.  They can be used at speaking events, conferences,
communications outreach efforts and donor relations materials (such as
press kits).  They should also be available on the Tor website in PDF
form.

Audience
--------

The audience is the general public - people with a wide range of
technical knowledge if any.  Also, most people have an attention span of
45 - 60 seconds when looking at a piece like this so getting across the
key messages is the focus.

Size
----

The brochures are designed to be 8.5 x 5.5 in or 215.9 x 139.7 mm -
basically 1/2 of a 8.5 x 11 in letter size sheet of paper so that people
can easily print them on one side of one sheet of paper.  The actual
page size contains an additional margin of 3 mm on each side typically
requested by print shops.  When printing these, use a heavier card stock
of at least 170 gsm.

Content
-------

Front: Each of the three brochures will have the same front.  The idea
is to have this be a quick glance about Tor.

Back:  There are three difference backs:

 1. Law Enforcement & The Tor Project: Geared as a quick reference for
    law enforcement audiences (not just investigators, but also support
    services).

 2. The Benefits of Anonymity Online: This is meant for journalists,
    domestic violence organizations, and others focused on protecting
    their identity online.

 3. Freedom & Privacy Online: The target audience here is the general
    public - helping educate people about the reasons that protecting
    their privacy is important.

Open discussion points
----------------------

 - Will people be confused who these Alice and Bob persons are?  Would
   they be less confused if we used a Tor Browser icon (whatever that
   would be) and a server/website icon?  Or would that make things too
   technical?

 - On the “Freedom & Privacy Online” slide, is “the regime” the only
   attacker, or is there a more generic term we might use?  And is there
   a less technical term for "Proxy" that we could use in the diagram
   next to it?

 - Is CC BY-NC-SA 4.0 Int. the license we want to use, or is BY-SA fine,
   too?

Technical note
--------------

Here's how to create or update QR codes on Debian:

  $ sudo apt-get install qrencode
  $ ls tor-brochure-*.pdf | cut -d'.' -f1 | \
    xargs -I{} qrencode -o {}.png -s 6 \
    'https://media.torproject.org/misc/2015-03-tor-brochure/'{}'.pdf'

